package com.team.hrayr.programmingtest;

import java.util.List;

/**
 * Created by Hrayr on 29/10/2018.
 */

public class NewsInfoWrapper {

    private boolean success;
    private List<Object> errors;
    private List<NewsInfo> data;

    public boolean isSuccess() {
        return success;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public List<NewsInfo> getData() {
        return data;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

    public void setData(List<NewsInfo> data) {
        this.data = data;
    }

}
