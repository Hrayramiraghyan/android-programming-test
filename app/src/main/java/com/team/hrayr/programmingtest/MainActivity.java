package com.team.hrayr.programmingtest;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            navigateToMainScreen(getSupportFragmentManager());
        }
    }

    public static void navigateToMainScreen(FragmentManager fragmentManager) {
        fragmentManager.beginTransaction()
                .add(R.id.container, MainFragment.newInstance())
                .commit();
    }

}
