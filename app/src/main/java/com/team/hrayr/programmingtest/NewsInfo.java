package com.team.hrayr.programmingtest;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hrayr on 27/10/2018.
 */

public class NewsInfo {

    @SerializedName("gallery")
    private List<Gallery> gallery;
    @SerializedName("title")
    private String title;
    @SerializedName("category")
    private String category;
    @SerializedName("date")
    private Long date;
    @SerializedName("coverPhotoUrl")
    private String coverPhotoUrl;
    @SerializedName("body")
    private String body;


    public NewsInfo(String title, String category, Long date, String coverPhotoUrl) {
        this.title = title;
        this.category = category;
        this.date = date;
        this.coverPhotoUrl = coverPhotoUrl;
    }

    public List<Gallery> getGallery() {
        return gallery;
    }

    public void setGallery(List<Gallery> gallery) {
        this.gallery = gallery;
    }


    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public Long getDate() {
        return date;
    }

    public String getCoverPhotoUrl() {
        return coverPhotoUrl;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public void setCoverPhotoUrl(String coverPhotoUrl) {
        this.coverPhotoUrl = coverPhotoUrl;
    }

    public static class Gallery {
        @SerializedName("title")
        private String title;

        @SerializedName("thumbnailUrl")
        private String thumbnailUrl;

        @SerializedName("contentUrl")
        private String contentUrl;

        public String getTitle() {
            return title;
        }

        public String getThumbnailUrl() {
            return thumbnailUrl;
        }

        public String getContentUrl() {
            return contentUrl;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setThumbnailUrl(String thumbnailUrl) {
            this.thumbnailUrl = thumbnailUrl;
        }

        public void setContentUrl(String contentUrl) {
            this.contentUrl = contentUrl;
        }
    }


}


