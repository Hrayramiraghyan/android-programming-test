package com.team.hrayr.programmingtest;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.team.hrayr.programmingtest.network.DataRequestClass;
import com.team.hrayr.programmingtest.network.RetrofitCallback;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainFragment extends Fragment {

    private final DataRequestClass dataRequestClass = new DataRequestClass();
    private RecyclerView recyclerView;
    private NewsAdapter adapter;
    ProgressDialog progressDialog;
    Picasso.Builder builder;


    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initProgressbar();
        initNetworkParse(view);
        return view;
    }

    private void initProgressbar() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading....");
        progressDialog.show();
    }


    public void initRecyclerViewAdapter(List<NewsInfo> news, View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        builder = new Picasso.Builder(getContext());
        builder.downloader(new OkHttp3Downloader(getContext()));
        adapter = new NewsAdapter((ArrayList<NewsInfo>) news, getContext(), builder, new ItemClickListener() {
            @Override
            public void onItemClicked(NewsInfo info) {
                NewsDetailFragment detail = NewsDetailFragment.newInstance(info);
                createFragment(detail, true);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

    }


    private void createFragment(Fragment fragment, boolean addToStack) {
        try {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            if (addToStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void initNetworkParse(final View view) {
        dataRequestClass.getData(new RetrofitCallback<NewsInfoWrapper>() {
            @Override
            public void onSuccess(NewsInfoWrapper newsInfoWrapper) {
                progressDialog.dismiss();
                List<NewsInfo> newsInfoList = newsInfoWrapper.getData();
                initRecyclerViewAdapter(newsInfoList, view);
            }

            @Override
            public void onError(Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

