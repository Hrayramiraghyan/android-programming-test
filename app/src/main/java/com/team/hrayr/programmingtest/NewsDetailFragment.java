package com.team.hrayr.programmingtest;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

public class NewsDetailFragment extends Fragment {

    private final static String EXTRA_KEY_DATA = "extra_bundle_data";

    private TextView detailsDate;
    private TextView detailsCategory;
    private TextView detailsTitle;
    private TextView detailsBody;
    private RecyclerView recyclerView;
    private ImagesAdapter adapter;
    private NewsInfo newsInfo;
    private Picasso.Builder builder;

    public NewsDetailFragment() {
        // Required empty public constructor
    }

    public static NewsDetailFragment newInstance(NewsInfo newsInfo) {
        NewsDetailFragment fragment = new NewsDetailFragment();
        Bundle bundle = new Bundle();
        String json = new Gson().toJson(newsInfo);
        bundle.putString(EXTRA_KEY_DATA, json);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String jsonStr = getArguments().getString(EXTRA_KEY_DATA);
            newsInfo = new Gson().fromJson(jsonStr, NewsInfo.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_detail, container, false);
        initViews(view); // find all views
        initRecyclerviewAdapter();
        initData(); // set parameters to views if needed (eg. tv.setText(newsInfo.getTitle))
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        initListeners(); // setup listeners lick onClick
    }

    private void initViews(View view) {
        detailsDate = view.findViewById(R.id.details_date);
        detailsCategory = view.findViewById(R.id.details_category);
        detailsTitle = view.findViewById(R.id.details_title);
        detailsBody = view.findViewById(R.id.details_body);
        recyclerView = view.findViewById(R.id.image_recycler_view);
    }

    private void initData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy ");
        detailsCategory.setText(newsInfo.getCategory());
        detailsDate.setText(simpleDateFormat.format(newsInfo.getDate() * 1000));
        detailsTitle.setText(newsInfo.getTitle());
        detailsBody.setText(newsInfo.getBody());
    }

    private void initRecyclerviewAdapter() {
        builder = new Picasso.Builder(getContext());
        builder.downloader(new OkHttp3Downloader(getContext()));
        adapter = new ImagesAdapter(newsInfo.getGallery(), getContext(), builder);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
    }

}
