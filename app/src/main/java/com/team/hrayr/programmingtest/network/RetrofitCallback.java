package com.team.hrayr.programmingtest.network;

/**
 * Created by Hrayr on 01/11/2018.
 */

public interface RetrofitCallback<T> {

    /**
     * Called when data got
     * @param t data
     */
    void onSuccess(T t);

    /**
     * Called when an error occurred during data getting process
     * @param t occurred error
     */
    void onError(Throwable t);
}
