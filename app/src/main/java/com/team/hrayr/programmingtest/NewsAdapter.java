package com.team.hrayr.programmingtest;

import android.content.Context;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


/**
 * Created by Hrayr on 27/10/2018.
 */


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    private ArrayList<NewsInfo> news;
    private Context mContext;
    private ItemClickListener listener;

    Picasso.Builder builder;

    public NewsAdapter(ArrayList<NewsInfo> news, Context mContext, Picasso.Builder builder, ItemClickListener listener) {
        this.news = news;
        this.mContext = mContext;
        this.builder = builder;
        this.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ArrayList<NewsInfo> newsInfos = new ArrayList<>();
        public TextView title, category, date;
        public ImageView coverPhoto;
        Context mContext;

        public MyViewHolder(View view, Context mContext, ArrayList<NewsInfo> news) {
            super(view);
            this.newsInfos = news;
            this.mContext = mContext;
            title = view.findViewById(R.id.title);
            category = view.findViewById(R.id.category);
            date = view.findViewById(R.id.date);
            coverPhoto = view.findViewById(R.id.cover_photo);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            NewsInfo newsInfo = this.newsInfos.get(position);
            listener.onItemClicked(newsInfo);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_row, parent, false);
        return new MyViewHolder(itemView, mContext, news);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy ");
        final NewsInfo info = news.get(position);
        holder.title.setText(info.getTitle());
        holder.category.setText(info.getCategory());
        holder.date.setText(simpleDateFormat.format(info.getDate() * 1000));
        builder.build().load(info.getCoverPhotoUrl())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.coverPhoto);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }
}