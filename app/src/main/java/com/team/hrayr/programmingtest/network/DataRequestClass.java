package com.team.hrayr.programmingtest.network;

import com.team.hrayr.programmingtest.GetDataService;
import com.team.hrayr.programmingtest.NewsInfoWrapper;
import com.team.hrayr.programmingtest.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Hrayr on 01/11/2018.
 */

public class DataRequestClass {

    private final Retrofit client = RetrofitClientInstance.getRetrofitInstance();

    public void getData(final RetrofitCallback<NewsInfoWrapper> callback) {
        GetDataService service = client.create(GetDataService.class);
        Call<NewsInfoWrapper> call = service.getAllInfo();
        call.enqueue(new Callback<NewsInfoWrapper>() {
            @Override
            public void onResponse(Call<NewsInfoWrapper> call, Response<NewsInfoWrapper> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<NewsInfoWrapper> call, Throwable t) {
                callback.onError(t);
            }
        });
    }
}

