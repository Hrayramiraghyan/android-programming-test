package com.team.hrayr.programmingtest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hrayr on 30/10/2018.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {
    private List<NewsInfo.Gallery> urls;
    private Context mContext;
    private Picasso.Builder builder;


    public ImagesAdapter(List<NewsInfo.Gallery> urls, Context mContext, Picasso.Builder builder) {
        this.urls = urls;
        this.mContext = mContext;
        this.builder = builder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        List<NewsInfo.Gallery> urls = new ArrayList<>();
        public ImageView coverPhoto;
        Context mContext;


        public MyViewHolder(View view, Context mContext, List<NewsInfo.Gallery> urls) {
            super(view);
            this.urls = urls;
            this.mContext = mContext;
            coverPhoto = view.findViewById(R.id.iv_cover_photo);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.details_item_list_row, parent, false);
        return new ImagesAdapter.MyViewHolder(itemView, mContext, urls);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (urls != null) {
            final NewsInfo.Gallery url = urls.get(position);
            builder.build().load(url.getThumbnailUrl())
                    .placeholder((R.drawable.ic_launcher_background))
                    .error(R.drawable.ic_launcher_background)
                    .into(holder.coverPhoto);
        }

    }

    @Override
    public int getItemCount() {
        if (urls == null) {
            return 0;
        } else {
            return urls.size();

        }
    }
}
