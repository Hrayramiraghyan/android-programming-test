package com.team.hrayr.programmingtest;



public interface ItemClickListener {
    void onItemClicked(NewsInfo info);
}
