package com.team.hrayr.programmingtest;


import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Hrayr on 27/10/2018.
 */

public interface GetDataService {
    @GET("feed")
    Call <NewsInfoWrapper> getAllInfo();
}
